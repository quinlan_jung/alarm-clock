/*
 * alarm.h
 *
 *  Created on: 2013-05-05
 *      Author: Quinlan Jung
 */

#ifndef ALARM_H_
#define ALARM_H_

#include <stdio.h>
#include <regex.h>
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>

#define SCALE 4 //adjust the volume level for LAME

//adjust the size of various string arrays
#define ERROR_BUFFER 100
#define LINE_BUFFER 100
#define FILE_BUFFER 100

extern FILE * f;

void play_music (char * path); //TODO Comment out for make test
int open_parse_txt(void);
int precompute_mp3(void);
int play_tasks(void);
int is_proper_regex (char * string_expr);
void music_handler (char * control_line);

#endif /* ALARM_H_ */
