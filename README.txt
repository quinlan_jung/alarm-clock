-------------------
    OVERVIEW
-------------------

This is an alarm clock I made for my raspberry pi. 
Here is what it does:
	1. Reads all Google Tasks due that day
	2. Reads daily greeting text files
	3. Plays a random mp3 file from music directory for a specified amount of time

-------------------
    DEPENDENCIES
-------------------

To make the code work, you will need to install the following:
libmpg123 -- http://www.mpg123.de/
libao -- http://www.xiph.org/ao/
lame -- http://lame.sourceforge.net/
*festival -- http://www.cstr.ed.ac.uk/projects/festival/

* I recommend installing the nitech hts voices instead of using the default festival voice 
because it sounds much better. At the time I made the project, the hts voices were not compatible
with festival and could only be run with a patched version at anonscm.debian.org/gitweb/?p=tts/festival.git

-------------------
    SETUP GOOGLE TASKS
-------------------

You will also need a Google Account to get your client id and secret to use the Tasks API.
Put the two pieces of info in ./python_tasks/client_secrets.json
https://developers.google.com/google-apps/tasks/

-------------------
    SETUP ALARM CLOCK
-------------------

I.) Put the text files you want written in ./text

II.) Put the music files you want played in ./music/xxx/ (any folder in the music directory)
 
III.) In the project directory, do the following:
 1. In ./precompute.c, change the variable parser_location to the proper directory 
 2. In ./python_tasks/client_secrets.json, add your client id and secret you got from Google

Then, in the project directory, type the following into a terminal:
 1. make clean
 2. make all
 3. ./precompute -p 
	This sets up ./control/parse.txt and specifies which text files and music you want played, and for how long
 4. ./precompute -t
 	This sets up the time you want to be woken up
