/*
 * main.c
 *
 *  Created on: 2013-03-07
 *      Author: teamawesome
 */
#include <stdio.h>
#include <time.h>
#include <string.h>

int main (void){

	time_t rawtime;
	struct tm * timeinfo;

    time (&rawtime);
	timeinfo = localtime (&rawtime);
	char * time_stamp = asctime(timeinfo);

	char * index;
	while (index = strstr(time_stamp, " ")){
		*index = '+';
	}


	FILE * savefile;

	if (!(savefile = fopen("test.txt", "w"))){
			return 1;
	}

	fprintf (savefile,"%s \n", time_stamp);

	if (fclose(savefile)){
			return 1;
		}


}
