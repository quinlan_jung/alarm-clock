/*
 * Precompute.c does two things:
 * ./precompute -p sets up parse.txt (run this first)
 * ./precompute -t sets up the crontab and makes the mp3's in codegen (run this second)
 *
 *  Created on: 2013-05-5
 *      Author: Quinlan Jung
 */


#include "alarm.h"

extern FILE * f;
char * parser_location = "/home/pi/pi/greeting.sh"; //TODO: change this to proper directory

/*Makes a line in parse.txt from option -p*/
void setup_parsetext(FILE * parsetext){
	char text[LINE_BUFFER];
	int before=0;
	int after=0;
	char before_text [LINE_BUFFER];
	char after_text [LINE_BUFFER];
	char music_file [LINE_BUFFER];
	char time [LINE_BUFFER];

	printf ("Do you want text spoken before music plays? (y/n)\n");
	while ( fgets(text, sizeof text, stdin) )
	{
		if (strcmp(text, "y\n")==0)
		{
			before = 1;
			printf("What is the name of the file you want spoken in the text dir? (ie) words.txt \n");
			fgets(before_text, LINE_BUFFER, stdin);
			break;
		}

		else if (strcmp(text,"n\n")==0){break;}

		else if (strcmp(text, "q\n")==0){
			printf("Goodbye, come again. \n");
			break;
		}

		else{
			fputs (text, stdout);
			fflush(stdout);
			fputs("Invalid entry. Type 'q' to quit.\n", stdout);
			printf ("Do you want text spoken before music plays? (y/n)\n");

		}
	}

	printf ("Do you want text spoken after music plays? (y/n)\n");
	while ( fgets(text, sizeof text, stdin) )
	{
		if (strcmp(text, "y\n")==0)
		{
			after = 1;
			printf("What is the name of the file you want spoken in the text dir? (ie) words.txt \n");
			fgets(after_text, LINE_BUFFER, stdin);
			break;
		}

		else if (strcmp(text,"n\n")==0){break;}

		else if (strcmp(text, "q\n")==0){
			printf("Goodbye, come again. \n");
			break;
		}

		else{
			fputs (text, stdout);
			fflush(stdout);
			fputs("Invalid entry. Type 'q' to quit.\n", stdout);
			printf ("Do you want text spoken after music plays? (y/n)\n");

		}
	}

	printf ("What is the name of the file where your music is stored in the music dir? \n");
	fgets(music_file, LINE_BUFFER, stdin);

	printf ("How long do you want your music to play for? (ie) 01:10 = 1 minute, 10 seconds \n");
	fgets(time, LINE_BUFFER, stdin);

	printf ("Here is the line that will be created: \n");
	before_text[strlen(before_text)-1] = '\0';
	after_text[strlen(after_text)-1] = '\0';
	music_file[strlen(music_file)-1] = '\0';
	time[strlen(time)-1] = '\0';

	printf ("MUSIC %s ", music_file);// MUSIC <path> (-b <path>)* 00:00
	if (before){
		printf ("-b %s ", before_text);
	}
	if (after){
		printf ("-a %s ", after_text);
	}
	printf ("%s \n", time);

	//write lines to parse.txt
	fprintf (parsetext, "MUSIC %s ", music_file);// MUSIC <path> (-b <path>)* 00:00
	if (before){
		fprintf (parsetext, "-b %s ", before_text);
	}
	if (after){
		fprintf (parsetext, "-a %s ", after_text);
	}
	fprintf (parsetext, "%s \n", time);

}

/*Creates all the lines in parse.txt from option -p*/
void manage_parsetext(){
	FILE * parsetext;
	char * parsename = "./control/parse.txt";
	parsetext = fopen(parsename, "w");
	int more_lines = 0;
	char text [LINE_BUFFER];
	do{
		more_lines =0;
		setup_parsetext(parsetext);
		printf ("Do you want another line in parsetext?\n");
		while ( fgets(text, sizeof text, stdin) )
		{
			if (strcmp(text, "y\n")==0)
			{
				more_lines =1;
				break;
			}

			else if (strcmp(text,"n\n")==0){break;}

			else if (strcmp(text, "q\n")==0){
				printf("Goodbye, come again. \n");
				break;
			}

			else{
				fputs (text, stdout);
				fflush(stdout);
				fputs("Invalid entry. Type 'q' to quit.\n", stdout);
				printf ("Do you want text spoken after music plays? (y/n)\n");

			}
		}
	}
	while(more_lines);
	fclose(parsetext);
}

/*Makes the cron file and runs it for the specified time in option -t*/
void setup_crontext(int hour, int minute){
	char * cron_filename = "./codegen/cron";
	FILE * cronfile;
	cronfile = fopen(cron_filename, "w");
	if (cronfile == NULL){
		printf("error: fopen\n");
		exit(1);
	}

	fprintf(cronfile,"%d %d * * * %s \n", minute, hour, parser_location);
	fclose(cronfile);
	chmod(cron_filename, S_IRWXU|S_IRWXG|S_IRWXO);
	system("crontab ./codegen/cron");
}

/*Sets up the crontab at the specified time in option -t*/
void setup_time()
{
   char text[LINE_BUFFER];
   fputs("What time do you want to wake up? (ie) 15:00, 8:00 \n", stdout);
   fflush(stdout);
   while ( fgets(text, sizeof text, stdin) )
   {
      int hour;
      int min;

      if ( sscanf(text, "%d:%d\n", &hour, &min) == 2 )
      {
         printf("Time is now set to %s\n", text);
         setup_crontext (hour, min);
         break;
      }
      else if (strcmp(text, "q\n")==0){
    	  printf("Goodbye, come again. \n");
    	  break;
      }

      else{
    	  fputs (text, stdout);
    	  fflush(stdout);
    	  fputs("Invalid entry. Type 'q' to quit.\n", stdout);
    	  fputs("What time do you want to wake up? (ie) 15:00, 08:00 \n ", stdout);

      }
   }
}

/*Prints proper usage when program receives improper args*/
void proper_usage(){
	printf ("Usage: ./precompute [switches] \n");
	printf ("Switches \n");
	printf ("\t -t ... setup time \n");
	printf ("\t -p ... modify alarm greeting \n");

}

/*Sets up parse.txt (-p) and the crontab with computed mp3's (-t)*/
int main(int argc, char * argv[])
{
	if (argc != 2){
		proper_usage();
	}
	else if (!strcmp(argv[1], "-t")) {
		setup_time();
		open_parse_txt();
		precompute_mp3();
		fclose(f);
	}

	else if (!strcmp(argv[1], "-p")) {
		manage_parsetext();
	}

	else{
		proper_usage();
	}
	return 0;
}

