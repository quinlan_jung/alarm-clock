# 
#   Created on: 2013-05-5
#       Author: Quinlan Jung
 

CFLAGS = -g

# Makes greeting and precompute executables with the libraries
all: parser.c music.c greeting.c precompute.c alarm.h
	gcc ${CFLAGS} -o greeting parser.c greeting.c music.c -lmpg123 -lao
	gcc ${CFLAGS} -o precompute parser.c precompute.c music.c -lmpg123 -lao

# Comment out the TODO's in parser.c and alarm.h to compile without the libraries (ie) on a different machine
test: parser.c greeting.c precompute.c alarm.h
	gcc ${CFLAGS} -o greeting parser.c greeting.c
	gcc ${CFLAGS} -o precompute parser.c precompute.c

# Removes .DS_Store files and prior executables	
clean:
	-find . -name ".DS_Store" -print0 | xargs -0 rm -rf
	-rm -rf greeting
	-rm -rf precompute