/*
 * Greeting.c parses the parse.txt file and plays the precomputed mp3's in codegen
 * (Must have run ./precompute -t to set up the crontab and run lame first)
 *
 *  Created on: 2013-05-5
 *      Author: Quinlan Jung
 */



#include "alarm.h"

extern FILE * f;

int main (void){

	open_parse_txt();

	//read the Google Tasks to be done today
	play_tasks();

	// check if each line in parse.txt matches the regex and play the requested mp3's
	char control_line [LINE_BUFFER];
	while (fgets(control_line, LINE_BUFFER-1, f) != NULL){
		if (!is_proper_regex(control_line)){continue;}

		if (strstr(control_line, "MUSIC ")== control_line){
			music_handler (control_line);
		}

	}

	//close parse.txt
	fclose(f);


	return 0;
}
