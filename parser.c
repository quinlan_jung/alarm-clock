/*
 * Parser.c contains all the methods to parse parse.txt and to compute the mp3's
 *
 *  Created on: 2013-05-5
 *      Author: Quinlan Jung
 */

#include "alarm.h"
FILE * f;
FILE * shellscript;
struct tm * timeinfo;

/*Return 1 if regex couldnt compile properly
Return 0 on success*/
int compile_regex (regex_t * r, const char * regex_text){
    int status = regcomp (r, regex_text, REG_EXTENDED|REG_NEWLINE);
    if (status != 0) {
	char error_message[ERROR_BUFFER];
	regerror (status, r, error_message, ERROR_BUFFER);
        printf ("Regex error compiling '%s': %s\n",
                 regex_text, error_message);
        return 1;
    }
    return 0;
}

/*Return 0 if no match
Return 1 otherwise*/
int match_regex (regex_t * r, const char * to_match){
    /* "P" is a pointer into the string which points to the end of the
       previous match. */
    const char * p = to_match;
    /* "N_matches" is the maximum number of matches allowed. */
    const int n_matches = 1;
    /* "M" contains the matches found. */
    regmatch_t m[n_matches];

    int i = 0;
    int nomatch = regexec (r, p, n_matches, m, 0);
    if (nomatch) {
    	return 0;
    }
    for (i = 0; i < n_matches; i++) {
    	if (m[i].rm_so == -1) {
    		break;
    	}
    }
    return 1;
}

/*Check if a line in parse.txt matches the regex*/
int is_proper_regex (char * string_expr){
	char * regex_music = "MUSIC [^ ]+ (-[a-z] [^ ]* )*[0-9][0-9]:[0-9][0-9]"; // MUSIC <path> (-b <path>)* 00:00
	regex_t rs;
	compile_regex(&rs, regex_music);
	return match_regex(&rs, string_expr);
}

/*Count the files in a directory*/
void count_files (int * file_count, DIR ** dp){
	struct dirent *ep;
	while ((ep = readdir (*dp))){
	    	if (ep->d_type == DT_REG){
	    		(*file_count)++;
	    	}
	    }
}

/*Generate a random number from 0 to file_count*/
int generate_rand_num (int * file_count){
	if (file_count == 0){
		return -1;
	}
	srand(time(NULL));
	return rand() % (*file_count);

}

/*Get a random file from dir dp and put the path in music_file_path*/
void get_rand_file(int * file_count, DIR ** dp, char * music_file_path){
	struct dirent *ep;
	int i = 0;
	int rand_num = generate_rand_num(file_count);
	while ((ep=readdir(*dp))){
		if (ep->d_type == DT_REG){
			if (i == rand_num){
				strcat(music_file_path, ep->d_name);
				return;
			}
			i++;
		}
	}
}

/*Get full path of random file*/
void extract_dir_get_rand_file(char * control_line, char * music_file_path){
	char * dir_name;
	strtok(control_line, " "); //MUSIC
	dir_name = strtok(NULL, " ");//MUSIC DIR NAME

	//MAKE FULL_PATH TO MUSIC FILE
	int path_length = strlen("./music/") + strlen(dir_name) + 1;
	char full_path [path_length];
	strcpy (full_path, "./music/");
	strcat(full_path, dir_name);

	//APPEND TO MUSIC_FILE_PATH
	strcpy(music_file_path, full_path);
	strcat(music_file_path, "/");

	//CHOOSE RANDOM FILE IN DIR
	DIR *dp;
	int file_count = 0;

	dp = opendir (full_path);

	if (dp != NULL)
	{
		count_files(&file_count, &dp);
		rewinddir(dp);
		get_rand_file(&file_count, &dp, music_file_path);

		(void) closedir (dp);
	}
	else
		printf ("Couldn't open the music directory \n");
}

/*Return 0 if file does not exist
Return 1 otherwise*/
FILE * is_valid_text_file (char * textfile){
	//MAKE FULL_PATH TO TEXT FILE
	int path_length = strlen("./text/")+strlen(textfile)+1;
	char full_path [path_length];
	strcpy (full_path, "./text/");
	strcat(full_path, textfile);
	FILE * fp = fopen (full_path, "r");

	if (fp == NULL){
		printf("Invalid text file path \n");
	}

	return fp;
}

/*Puts the proper paths from control_line to before_path, after_path and time */
void extract_text_paths_and_time (char * control_line, char * before_text_path, char * after_text_path, char * time){
	char * next_arg = strtok(NULL, " ");
	while (strchr(next_arg, '-') == next_arg){ //check for -x arg
		if (strchr(next_arg, 'a')==next_arg+1){
			next_arg = strtok(NULL, " "); // get txt file name
			if(is_valid_text_file(next_arg)){//check if valid text file
				strcpy (after_text_path, "./text/");
				strcat (after_text_path, next_arg);
			}
		}
		else if (strchr(next_arg, 'b')==next_arg+1){
			next_arg = strtok(NULL, " "); // get txt file name
			if(is_valid_text_file(next_arg)){//check if valid text file
				strcpy (before_text_path, "./text/");
				strcat (before_text_path, next_arg);
			}
		}
		next_arg = strtok(NULL, " "); //get next arg
	}
	strcpy(time, next_arg);
}

void dummyHandler(int signal){}
void termHandler(){exit(0);}

/*Plays music until (1) mp3 has finished or (2) time specified in parse.txt expires*/
void play_music_for_some_time(char * music_file_path, char * time){
	int minutes = 0;
	int seconds =0;
	sscanf(time, "%d:%d", &minutes, &seconds);
	struct timespec t;
	t.tv_sec = minutes*60 + seconds;
	t.tv_nsec = 0;

	signal(SIGCHLD,dummyHandler);
	int child_pid = fork();
	if (child_pid == 0){//child
		signal (SIGTERM, termHandler);
		play_music(music_file_path); //TODO Comment out for make test
		exit(0);
	}

	else{ //parent
		nanosleep(&t, NULL); //will be woken up with dummy handler via sigchld
		signal(SIGCHLD, SIG_IGN); // if timer expires before music finishes, kill child
		kill (child_pid, SIGTERM);
		wait(NULL);
	}
}

// NOT USED -- generates scheme file for festival
int generate_scm_file(char * voice, char * text_path){
	FILE * say_text;

	if (!(say_text = fopen("./codegen/say_text.scm", "w"))){
		return -1;
	}

	fprintf (say_text, "(voice_%s) \n", voice); //set voice
	char * raw_file = strrchr(text_path, '/');
	fprintf (say_text, "(tts_file \"./codegen/%s.mp3\" nil) \n", raw_file); //say file

	if (fclose(say_text)){
		return -1;
	}
	return 1;
}

// NOT USED -- gets festival to read a line we put into a scheme file with proper voice
void read_text (char * text_path){

	generate_scm_file("nitech_us_slt_arctic_hts", text_path); // generate scheme file to say text
	pid_t   childpid;

	if((childpid = fork()) == -1)
	{
		perror("fork");
		exit(1);
	}

	if(childpid == 0) // child calls festival to use .scm file
	{
		char *const parmList[] = {"/usr/bin/festival", "-b", "./codegen/say_text.scm", NULL};
		execv ("/usr/bin/festival", parmList);
		exit(0);
	}
	else{
		wait(NULL);
		printf ("child has returned from festival \n");
	} // wait for child to return
}


/*Parses a line from parse.txt and plays music and text files */
void music_handler (char * control_line){
	char music_file_path [FILE_BUFFER];
	char before_text_path [FILE_BUFFER];
	char after_text_path [FILE_BUFFER];
	char time [LINE_BUFFER];
	char buffer [FILE_BUFFER];

	strcpy (before_text_path, "\0");
	strcpy (after_text_path, "\0");

	extract_dir_get_rand_file(control_line, music_file_path);
	extract_text_paths_and_time (control_line, before_text_path, after_text_path, time);

	char * raw_file;
	if (strlen(before_text_path) != 0){
		raw_file = strrchr(before_text_path, '/');
		strcpy(buffer, "./codegen/");
		strcat(buffer, raw_file);
		strcat (buffer, ".mp3");
		play_music(buffer); //TODO Comment out for make test
	}

	play_music_for_some_time(music_file_path, time); //TODO Comment out for make test

	if (strlen(after_text_path) != 0){
		raw_file = strrchr(after_text_path, '/');
		strcpy(buffer, "./codegen/");
		strcat(buffer, raw_file);
		strcat (buffer, ".mp3");
		play_music(buffer); //TODO Comment out for make test
	}
}

void initialize_time(){
	time_t rawtime;
	time (&rawtime);
	timeinfo = localtime (&rawtime);
}

/*Makes the mp3 file from a line in parse.txt*/
void music_handler_precompute (char * control_line){
	char music_file_path [FILE_BUFFER];
	char before_text_path [FILE_BUFFER];
	char after_text_path [FILE_BUFFER];
	char time [LINE_BUFFER];
	char buffer [LINE_BUFFER];

	initialize_time();

	strcpy (before_text_path, "\0");
	strcpy (after_text_path, "\0");

	extract_dir_get_rand_file(control_line, music_file_path);
	extract_text_paths_and_time (control_line, before_text_path, after_text_path, time);

	char * raw_file;
	if (strlen(before_text_path) != 0){
		raw_file = strrchr(before_text_path, '/');
		strftime (buffer, LINE_BUFFER, "By the way, the time is now %r", timeinfo);
		fprintf (shellscript, "(cat %s; echo \" %s \")| text2wave -o ./codegen%s.wav \n", before_text_path, buffer, raw_file);
		fprintf (shellscript, "lame --scale %d ./codegen%s.wav ./codegen%s.mp3 \n", SCALE, raw_file, raw_file);
	}

	int minutes = 0;
	int seconds =0;
	sscanf(time, "%d:%d", &minutes, &seconds);
	timeinfo->tm_min += minutes;
	timeinfo->tm_sec += seconds;
	mktime(timeinfo);

	if (strlen(after_text_path) != 0){
		raw_file = strrchr(after_text_path, '/');
		strftime (buffer, LINE_BUFFER, "By the way, the time is now %r \n", timeinfo);
		fprintf (shellscript, "(cat %s; echo \" %s \")| text2wave -o ./codegen/%s.wav \n", after_text_path, buffer, raw_file);
		fprintf (shellscript, "lame --scale %d ./codegen/%s.wav ./codegen/%s.mp3 \n", SCALE, raw_file, raw_file);
	}
}


/*Makes all the mp3 file based on the given text files and puts it in codegen*/
int precompute_mp3(){
	//OPEN SHELLSCRIPT
	printf ("Please wait while the mp3's are being generated...\n");
	char * shellname = "./codegen/shellscript";
	shellscript = fopen (shellname, "w");
	if (shellscript == NULL){
		printf("error: fopen\n");
		return 1;
	}
	chmod(shellname, S_IRWXU|S_IRWXG|S_IRWXO);
	fprintf (shellscript, "#!/bin/bash \n");

	// PRECOMPUTE TEXT TO WAV
	char control_line [LINE_BUFFER];
	while (fgets(control_line, LINE_BUFFER-1, f) != NULL){
		if (!is_proper_regex(control_line)){continue;}

		if (strstr(control_line, "MUSIC ")== control_line){
			music_handler_precompute (control_line);
		}

	}

	fclose (shellscript);
	pid_t   childpid;

	if((childpid = fork()) == -1)
	{
		perror("fork");
		exit(1);
	}

	if(childpid == 0) // child calls festival to use .scm file
	{
		char *const parmList[] = {"./codegen/shellscript", NULL};
		execv ("./codegen/shellscript", parmList);
		exit(0);
	}
	else{ wait(NULL);}

	rewind(f);

	return 0;

}

/*Reads the Google Tasks due today*/
int play_tasks(){
	printf ("Grabbing the Google Tasks you have due today... \n");
	int success = system("./control/shellscript_google_tasks");
	if (success == -1){
		perror ("shellscript_google_tasks \n");
	}
	return success;
}

int open_parse_txt(){
	// OPEN PARSE.TXT
	char * filename = "./control/parse.txt";
	f = fopen(filename, "r");
	if (f == NULL){
		printf("error: fopen\n");
		return 1;
	}
	return 0;
}
